package ollama

import (
	"angry_ai/utils"
	"bytes"
	"encoding/json"
	"net/http"
)

type Ollama struct {
	client       http.Client
	apiURL       string
	Models       []string
	VisionModels []string
}

type Req struct {
	Model   string   `json:"model"`
	Prompt  string   `json:"prompt"`
	Context []int    `json:"context"`
	Images  []string `json:"images"`
	Stream  bool     `json:"stream"`
}

type Response struct {
	Model   string `jsom:"model"`
	Resp    string `json:"response"`
	Context []int  `json:"context"`
}

func New(config *utils.Config) Ollama {
	return Ollama{
		client:       http.Client{},
		apiURL:       config.ApiURL,
		Models:       config.Models,
		VisionModels: config.VisionModels,
	}
}

func (o *Ollama) Generate(model, prompt string, context []int) (Response, error) {
	apiURl := o.apiURL + "/api/generate"
	var r Response
	body := &Req{
		Model:   model,
		Prompt:  prompt,
		Stream:  false,
		Context: context,
	}
	jsonData, err := json.Marshal(body)
	if err != nil {
		return r, err
	}
	req, _ := http.NewRequest(http.MethodPost, apiURl, bytes.NewBuffer(jsonData))
	req.Header.Set("Content-Type", "application/json")

	resp, err := o.client.Do(req)

	if err != nil {
		return r, err
	}

	err = json.NewDecoder(resp.Body).Decode(&r)
	if err != nil {
		return r, err
	}

	return r, nil

}

func (o *Ollama) GenerateVison(model, prompt string, imageBase64 string, context []int) (Response, error) {
	apiURl := o.apiURL + "/api/generate"
	var r Response
	var imgs []string
	imgs = append(imgs, imageBase64)
	body := &Req{
		Model:   model,
		Prompt:  prompt,
		Stream:  false,
		Images:  imgs,
		Context: context,
	}
	jsonData, err := json.Marshal(body)
	if err != nil {
		return r, err
	}
	req, _ := http.NewRequest(http.MethodPost, apiURl, bytes.NewBuffer(jsonData))
	req.Header.Set("Content-Type", "application/json")

	resp, err := o.client.Do(req)

	if err != nil {
		return r, err
	}

	err = json.NewDecoder(resp.Body).Decode(&r)
	if err != nil {
		return r, err
	}

	return r, nil

}
