package main

import (
	"angry_ai/bot"
	"angry_ai/utils"
	"fmt"
	"net/http"

	_ "github.com/mattn/go-sqlite3"
)

func main() {
	c := utils.New()
	bot := bot.New(&c)

	err := http.ListenAndServe(":8080", http.HandlerFunc(bot.HandleTelegramWebHook))
	if err != nil {
		fmt.Println(err)
		return
	}
}
