package utils

import (
	"regexp"
	"strconv"
	"strings"
)

func EditForTg(text string) string {
	re := regexp.MustCompile(`[@_*]`)
	return re.ReplaceAllString(text, " ")
}

func ConvertIntSliceToString(s []int) string {
	var str []string
	for _, v := range s {
		str = append(str, strconv.Itoa(v))
	}
	return strings.Join(str, " ")
}

func ConvertStrToIntSlice(s string) []int {
	str := strings.Split(s, " ")
	var res []int
	for _, v := range str {
		i, err := strconv.Atoi(v)
		if err != nil {
			return nil
		}
		res = append(res, i)
	}
	return res
}
