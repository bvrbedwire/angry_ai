package utils

import (
	"log"
	"os"

	"gopkg.in/yaml.v3"
)

type Config struct {
	ApiURL       string   `yaml:"ollama-api-URL"`
	Models       []string `yaml:"models"`
	VisionModels []string `yaml:"vision-models"`

	TgAdmin    int    `yaml:"admin-id"`
	TgBotToken string `yaml:"bot-token"`
	TgAPIURL   string `yaml:"tg-api-URL"`

	DBName string `yaml:"db-name"`
	DBPath string `yaml:"db-path"`
}

func New() Config {
	yamlFile, err := os.ReadFile("config.yaml")
	if err != nil {
		log.Fatal(err)
	}

	var config Config
	err = yaml.Unmarshal(yamlFile, &config)
	if err != nil {
		log.Fatal(err)
	}

	return config
}
