package bot

import (
	"angry_ai/db"
	"angry_ai/ollama"
	"angry_ai/utils"
	"bytes"
	"encoding/base64"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"net/url"
	"strconv"
	"strings"
)

type Bot struct {
	admin       int
	apiURL      string
	token       string
	client      http.Client
	ollama      ollama.Ollama
	chatForWait map[int]bool
	db          db.DB
}

func New(config *utils.Config) Bot {
	var c = make(map[int]bool)
	ollama := ollama.New(config)
	db := db.New(config.DBName, config.DBPath, ollama.Models)
	db.CreateTables()
	db.AddSettingsParams()
	db.AddAdmin(config.TgAdmin, "admin")
	return Bot{
		admin:       config.TgAdmin,
		apiURL:      config.TgAPIURL,
		token:       config.TgBotToken,
		client:      http.Client{},
		ollama:      ollama,
		chatForWait: c,
		db:          db,
	}
}

func (b *Bot) parseTeleramRequest(r *http.Request) (*Update, error) {
	var update Update
	err := json.NewDecoder(r.Body).Decode(&update)
	if err != nil {
		return nil, err
	}
	return &update, nil
}

func (b *Bot) HandleTelegramWebHook(w http.ResponseWriter, r *http.Request) {
	update, err := b.parseTeleramRequest(r)
	if err != nil {
		return
	}
	chatID := update.Message.Chat.Id

	if update.CallbackQuery != nil {
		if update.CallbackQuery.Message.Chat.Id == b.admin {
			b.processAdminCallbackQuery(update)
			return
		} else {
			b.processUserCallbackQuery(update)
			return
		}
	}

	if chatID == b.admin {
		b.HandlerAdmin(update)
		return
	} else {
		b.HandlerUser(update)
		return
	}

}

func (b *Bot) processQuery(update *Update, model string) {
	chatID := update.Message.Chat.Id
	if update.Message.Text == "" && update.Message.Photo == nil {
		b.sendMessage(chatID, "Непонятный запрос...", nil)
		return
	}

	if len(update.Message.Photo) > 0 {
		for _, v := range b.ollama.VisionModels {
			if model == v {
				b.processImageQuery(update, model)
				return
			}
		}
		b.sendMessage(chatID, "Выберете vision модель из: "+strings.Join(b.ollama.VisionModels, ", "), nil)
		return
	}

	if update.Message.Text != "" {
		b.processChatQuery(update, model)
	}

}

func (b *Bot) processChatQuery(update *Update, model string) {
	chatID := update.Message.Chat.Id
	prompt := update.Message.Text
	b.chatForWait[chatID] = true
	b.db.PlusRequest(chatID)
	lastMessage, err := b.sendMessage(chatID, "Запрос в обработке...", nil)
	if err != nil {
		b.chatForWait[chatID] = false
	}
	resp, err := b.ollama.Generate(model, prompt, b.db.GetContext(chatID))
	if err != nil {
		b.chatForWait[chatID] = false
	}
	err = b.removeLastMessage(chatID, lastMessage.ID)
	if err != nil {
		b.chatForWait[chatID] = false
	}
	if b.db.IsContext(chatID) {
		b.db.SetContext(chatID, resp.Context)
	}
	_, err = b.sendMessage(chatID, resp.Resp, nil)
	if err != nil {
		b.chatForWait[chatID] = false
	}

	b.chatForWait[chatID] = false
}

func (b *Bot) processImageQuery(update *Update, model string) {
	chatID := update.Message.Chat.Id
	text := update.Message.Caption
	image, err := b.GetImageBase64(update.Message.Photo)
	if err != nil {
		return
	}

	b.chatForWait[chatID] = true
	b.db.PlusRequest(chatID)
	lastMessage, err := b.sendMessage(chatID, "Запрос в обработке...", nil)
	if err != nil {
		b.chatForWait[chatID] = false
	}

	resp, err := b.ollama.GenerateVison(model, text, image, b.db.GetContext(chatID))
	if err != nil {
		b.chatForWait[chatID] = false
	}

	err = b.removeLastMessage(chatID, lastMessage.ID)
	if err != nil {
		b.chatForWait[chatID] = false
	}

	if b.db.IsContext(chatID) {
		b.db.SetContext(chatID, resp.Context)
	}

	_, err = b.sendMessage(chatID, resp.Resp, nil)
	if err != nil {
		b.chatForWait[chatID] = false
	}

	b.chatForWait[chatID] = false
}

func (b *Bot) sendMessage(chatID int, text string, replyMarkup *ReplyMarkup) (Message, error) {
	messageData := &Message{
		ChatID:         chatID,
		Text:           text,
		ParseMode:      "Markdown",
		DisablePreview: true,
		ReplyMarkup:    replyMarkup,
	}
	var response Resp
	response.Message = messageData

	jsonData, err := json.Marshal(messageData)
	if err != nil {
		return *response.Message, err
	}
	req, err := http.NewRequest(http.MethodPost, b.apiURL+b.token+"/sendMessage", bytes.NewBuffer(jsonData))
	if req == nil {
		return *response.Message, err
	}
	req.Header.Set("Content-Type", "application/json")
	resp, err := b.client.Do(req)
	if err != nil {
		return *response.Message, err
	}

	err = json.NewDecoder(resp.Body).Decode(&response)
	if err != nil {
		return *response.Message, err
	}

	return *response.Message, nil
}

func (b *Bot) removeLastMessage(chatID, messageID int) error {
	_, err := http.PostForm(
		b.apiURL+b.token+"/deleteMessage",
		url.Values{
			"chat_id":    {strconv.Itoa(chatID)},
			"message_id": {strconv.Itoa(messageID)},
		})

	if err != nil {
		return err
	}
	return nil
}

func (b *Bot) registerUser(id int, username, token string) {
	b.db.AddUser(id, username)
	b.db.DeleteToken(token)
}

func (b *Bot) GetImageBase64(p []Photo) (string, error) {
	fileResp, err := http.Get(b.apiURL + b.token + "/getFile?file_id=" + p[len(p)-1].FileID)
	if err != nil {
		return "", err
	}
	defer fileResp.Body.Close()

	fileBody, err := ioutil.ReadAll(fileResp.Body)
	if err != nil {
		return "", err
	}
	var fileResponse struct {
		Ok     bool `json:"ok"`
		Result struct {
			FileID       string `json:"file_id"`
			FileUniqueID string `json:"file_unique_id"`
			FileSize     int    `json:"file_size"`
			FilePath     string `json:"file_path"`
		} `json:"result"`
	}
	err = json.Unmarshal(fileBody, &fileResponse)
	if err != nil {
		return "", err
	}

	imageURL := fmt.Sprintf("https://api.telegram.org/file/bot%s/%s", b.token, fileResponse.Result.FilePath)
	imageResp, err := http.Get(imageURL)
	if err != nil {
		return "", err
	}
	defer imageResp.Body.Close()

	imageData, err := ioutil.ReadAll(imageResp.Body)
	if err != nil {
		return "", err
	}
	imageBase64 := base64.StdEncoding.EncodeToString(imageData)
	return imageBase64, nil
}
