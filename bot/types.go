package bot

type CallbackQuery struct {
	Data    string   `json:"data"`
	From    *User    `json:"from"`
	Message *Message `json:"message"`
}

type Update struct {
	UpdateId      int            `json:"update_id"`
	Message       Message        `json:"message"`
	CallbackQuery *CallbackQuery `json:"callback_query"`
}

type Message struct {
	ID             int          `json:"message_id"`
	From           *User        `json:"from"`
	ChatID         int          `json:"chat_id"`
	Text           string       `json:"text"`
	Chat           Chat         `json:"chat"`
	ParseMode      string       `json:"parse_mode,omitempty"`
	DisablePreview bool         `json:"disable_web_page_preview,omitempty"`
	ReplyMarkup    *ReplyMarkup `json:"reply_markup,omitempty"`
	Photo          []Photo      `json:"photo"`
	Caption        string       `josn:"caption"`
}

type Chat struct {
	Id int `json:"id"`
}

type User struct {
	ID       int    `json:"id"`
	Username string `json:"username"`
}

type Resp struct {
	OK      bool     `json:"ok"`
	Message *Message `json:"result"`
}

type Photo struct {
	FileID   string `json:"file_id"`
	FileSize int    `json:"file_size"`
	Width    int    `json:"width"`
	Height   int    `json:"height"`
}
