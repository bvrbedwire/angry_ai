package bot

func (b *Bot) HandlerUser(update *Update) {
	chatID := update.Message.Chat.Id
	text := update.Message.Text

	if b.db.IsActiveUser(chatID) {
		if !b.db.IsEnableForUsers() {
			return
		}

		if b.chatForWait[chatID] {
			b.sendMessage(chatID, "Предыдущий запрос в обработке.", nil)
			return
		}

		switch text {
		case "/start":
			b.sendMessage(chatID, WARRNING_MESSAGE, nil)
			b.sendMessage(chatID, WELCOME_MESSAGE, nil)
			return
		case "/settings":
			b.sendMessage(chatID, "Настройки:", b.SettingsMarkup())
			return
		case "/info":
			info := b.db.UserInfo(chatID)
			b.sendMessage(chatID, info, nil)
			return
		}

		go b.processQuery(update, b.db.Model(chatID))
		return
	}

	if len(update.Message.Text) == 48 {
		if b.db.IsAvailableToken(update.Message.Text) {
			b.registerUser(chatID, update.Message.From.Username, update.Message.Text)
			b.sendMessage(chatID, WARRNING_MESSAGE, nil)
			b.sendMessage(chatID, WELCOME_MESSAGE, nil)
			return
		}
	}
}

func (b *Bot) processUserCallbackQuery(update *Update) {
	callbackData := update.CallbackQuery.Data
	chatID := update.CallbackQuery.Message.Chat.Id
	messageID := update.CallbackQuery.Message.ID

	switch callbackData {
	case u_main_menu:
		b.removeLastMessage(chatID, messageID)
		b.sendMessage(chatID, "Настройки:", b.SettingsMarkup())
		return
	}

	b.processSwitchContext(callbackData, chatID, messageID)
	b.processCallbackSettings(callbackData, chatID, messageID)
	b.processCallbackModels(callbackData, chatID, messageID)
}
