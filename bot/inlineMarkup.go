package bot

type InlineButton struct {
	Text         string `json:"text"`
	URL          string `json:"url"`
	CallbackData string `json:"callback_data"`
}

type ReplyMarkup struct {
	InlineKeyboard [][]InlineButton `json:"inline_keyboard"`
}

func inlineReplyMarkup(inlineButtons ...[]InlineButton) *ReplyMarkup {
	inlineKeyboard := [][]InlineButton{}
	inlineKeyboard = append(inlineKeyboard, inlineButtons...)
	replyMarkup := &ReplyMarkup{
		InlineKeyboard: inlineKeyboard,
	}
	return replyMarkup
}

func inlineButton(text, callbackData string) []InlineButton {
	return []InlineButton{
		{
			Text:         text,
			CallbackData: callbackData,
		},
	}
}
