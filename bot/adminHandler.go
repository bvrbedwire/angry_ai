package bot

import (
	"angry_ai/utils"
	"strconv"
	"strings"
)

const (
	a_add_token          = "a-add-token"
	a_all_tokens         = "a-all-tokens"
	a_disable_all_tokens = "a-disable_all-tokens"
	a_disable_user       = "a-disable-user"
	a_stop_for_users     = "a-stop-for-users"
	a_start_for_users    = "a-start_for_users"
	a_get_users          = "a-get-users"
)

func (b *Bot) HandlerAdmin(update *Update) {
	chatID := update.Message.Chat.Id
	text := update.Message.Text

	if b.chatForWait[chatID] {
		b.sendMessage(chatID, "Предыдущий запрос в обработке.", nil)
		return
	}

	switch text {
	case "/admin":
		b.sendMessage(update.Message.Chat.Id, "Menu:", adminStartInlineMarkup())
		return
	case "/model":
		b.sendMessage(chatID, "Выберете модель:", b.ModelsMarkup(chatID))
		return
	case "/settings":
		b.sendMessage(chatID, "Настройки:", b.SettingsMarkup())
		return
	case "/start":
		b.sendMessage(chatID, "Напишите запрос.", nil)
		return
	case "/info":
		info := b.db.UserInfo(chatID)
		info_a := "/admin\n/enable <uid>\n/disable <uid>"
		b.sendMessage(chatID, info, nil)
		b.sendMessage(chatID, info_a, nil)
		return
	}

	commands := strings.Split(text, " ")
	if len(commands) > 1 {

		switch commands[0] {
		case "/enable":
			uid, err := strconv.Atoi(commands[1])
			if err != nil {
				return
			}
			b.db.EnableUser(uid)
			return
		case "/disable":
			uid, err := strconv.Atoi(commands[1])
			if err != nil {
				return
			}
			b.db.DisableUser(uid)
			return
		}
	}

	go b.processQuery(update, b.db.Model(chatID))
}

func (b *Bot) processAdminCallbackQuery(update *Update) {
	callbackData := update.CallbackQuery.Data
	chatID := update.CallbackQuery.Message.Chat.Id
	messageID := update.CallbackQuery.Message.ID

	switch callbackData {
	case a_add_token:
		b.removeLastMessage(chatID, messageID)
		token, _ := utils.GenerateToken(24)
		b.db.AddToken(token)
		b.sendMessage(chatID, token, nil)
		b.sendMessage(chatID, "Menu:", adminStartInlineMarkup())
	case a_all_tokens:
		b.removeLastMessage(chatID, messageID)
		b.sendMessage(chatID, b.db.AllTokens(), nil)
		b.sendMessage(chatID, "Menu:", adminStartInlineMarkup())
	case a_disable_all_tokens:
		b.removeLastMessage(chatID, messageID)
		b.db.DisableAllTokens()
		b.sendMessage(chatID, "All TOKENS REMOVED", nil)
		b.sendMessage(chatID, "Menu:", adminStartInlineMarkup())
		return
	case a_stop_for_users:
		b.removeLastMessage(chatID, messageID)
		b.db.DisableForUsers()
		b.sendMessage(chatID, "Bot disabled for users", nil)
		b.sendMessage(chatID, "Menu:", adminStartInlineMarkup())
	case a_start_for_users:
		b.removeLastMessage(chatID, messageID)
		b.db.EnableForUsers()
		b.sendMessage(chatID, "Bot enabled for users", nil)
		b.sendMessage(chatID, "Menu:", adminStartInlineMarkup())
	case a_get_users:
		b.removeLastMessage(chatID, messageID)
		b.sendMessage(chatID, b.db.AllUsers(), nil)
		b.sendMessage(chatID, "Menu:", adminStartInlineMarkup())
	}

	switch callbackData {
	case u_main_menu:
		b.removeLastMessage(chatID, messageID)
		b.sendMessage(chatID, "Настройки:", b.SettingsMarkup())
		return
	}
	b.processSwitchContext(callbackData, chatID, messageID)
	b.processCallbackSettings(callbackData, chatID, messageID)
	b.processCallbackModels(callbackData, chatID, messageID)
}

func adminStartInlineMarkup() *ReplyMarkup {
	addToken := inlineButton("🔑Add TOKEN", a_add_token)
	allTokens := inlineButton("🔑All TOKENS", a_all_tokens)
	disableAllTokens := inlineButton("🔑DISABLE TOKENS", a_disable_all_tokens)
	stopBotForUsers := inlineButton("⏹Stop for users", a_stop_for_users)
	startBotForUsers := inlineButton("▶️Start for users", a_start_for_users)
	getUsers := inlineButton("👤Get users", a_get_users)
	replyMarkup := inlineReplyMarkup(addToken, allTokens, disableAllTokens, stopBotForUsers, startBotForUsers, getUsers)
	return replyMarkup
}
