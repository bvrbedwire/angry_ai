package bot

const (
	u_main_menu = "u-main-menu"

	U_SETTINGS_MODELS  = "u_settings_models"
	U_SETTINGS_CONTEXT = "u_settings_context"
	U_SETTINGS_EXIT    = "u_settings_exit"

	U_ENABLE_CONTEXT  = "u-enable-context"
	U_DISABLE_CONTEXT = "u-disable-context"
)

func (b *Bot) SettingsMarkup() *ReplyMarkup {
	models := inlineButton("🧠Модели", U_SETTINGS_MODELS)
	context := inlineButton("📝Контекст", U_SETTINGS_CONTEXT)
	backBtn := inlineButton("< Выйти", U_SETTINGS_EXIT)
	replyMarkup := inlineReplyMarkup(models, context, backBtn)
	return replyMarkup

}

func (b *Bot) ContextMarkup(id int) *ReplyMarkup {
	on := "on"
	off := "off"
	if b.db.IsContext(id) {
		on = "✅" + on
	} else {
		off = "✅" + off
	}
	models := inlineButton(on, U_ENABLE_CONTEXT)
	context := inlineButton(off, U_DISABLE_CONTEXT)
	backBtn := inlineButton("< назад", u_main_menu)
	replyMarkup := inlineReplyMarkup(models, context, backBtn)
	return replyMarkup
}

func (b *Bot) ModelsMarkup(id int) *ReplyMarkup {
	var ib [][]InlineButton
	for _, v := range b.ollama.Models {
		btnName := v
		if b.db.Model(id) == v {
			btnName = "✅" + v
		}
		ib = append(ib, inlineButton(btnName, v))
	}
	for _, v := range b.ollama.VisionModels {
		btnName := v
		if b.db.Model(id) == v {
			btnName = "✅" + v
		}
		ib = append(ib, inlineButton(btnName, v))
	}
	backBtn := inlineButton("< назад", u_main_menu)
	ib = append(ib, backBtn)
	replyMarkup := inlineReplyMarkup(ib...)
	return replyMarkup
}

func (b *Bot) processCallbackSettings(callbackData string, chatID, messageID int) {
	switch callbackData {
	case U_SETTINGS_MODELS:
		b.removeLastMessage(chatID, messageID)
		b.sendMessage(chatID, "Выберете модель:", b.ModelsMarkup(chatID))
		return
	case U_SETTINGS_CONTEXT:
		b.removeLastMessage(chatID, messageID)
		b.sendMessage(chatID, "Контекст:", b.ContextMarkup(chatID))
		return
	case U_SETTINGS_EXIT:
		b.removeLastMessage(chatID, messageID)
	}
}

func (b *Bot) processSwitchContext(callbackData string, chatID, messageID int) {
	switch callbackData {
	case U_ENABLE_CONTEXT:
		b.removeLastMessage(chatID, messageID)
		b.db.EnableContext(chatID)
		b.sendMessage(chatID, "Контекст:", b.ContextMarkup(chatID))
	case U_DISABLE_CONTEXT:
		b.removeLastMessage(chatID, messageID)
		b.db.DisableContext(chatID)
		b.sendMessage(chatID, "Контекст:", b.ContextMarkup(chatID))
	}
}

func (b *Bot) processCallbackModels(callbackData string, chatID, messageID int) {
	for _, v := range b.ollama.Models {
		if callbackData == v {
			b.removeLastMessage(chatID, messageID)
			b.db.ChangeModel(chatID, v)
			b.sendMessage(chatID, "Выберете модель:", b.ModelsMarkup(chatID))
		}
	}
	for _, v := range b.ollama.VisionModels {
		if callbackData == v {
			b.removeLastMessage(chatID, messageID)
			b.db.ChangeModel(chatID, v)
			b.sendMessage(chatID, "Выберете модель:", b.ModelsMarkup(chatID))
		}
	}
}
