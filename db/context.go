package db

import (
	"angry_ai/utils"
	"database/sql"

	_ "github.com/mattn/go-sqlite3"
)

func (d *DB) EnableContext(id int) error {
	db, err := sql.Open("sqlite3", d.path+d.name)
	if err != nil {
		return err
	}
	defer db.Close()

	q := `UPDATE users SET is_context = 1 WHERE id = ?;`
	_, err = db.Exec(q, id)
	if err != nil {
		return err
	}

	return nil
}

func (d *DB) DisableContext(id int) error {
	db, err := sql.Open("sqlite3", d.path+d.name)
	if err != nil {
		return err
	}
	defer db.Close()

	q := `UPDATE users SET is_context = 0 WHERE id = ?;`
	_, err = db.Exec(q, id)
	if err != nil {
		return err
	}

	d.ClearContext(id)

	return nil
}

func (d *DB) ClearContext(id int) error {
	db, err := sql.Open("sqlite3", d.path+d.name)
	if err != nil {
		return err
	}
	defer db.Close()
	q := `UPDATE users SET context = null WHERE id = ?;`
	_, err = db.Exec(q, id)
	if err != nil {
		return err
	}
	return nil
}

func (d *DB) GetContext(id int) []int {
	db, err := sql.Open("sqlite3", d.path+d.name)
	if err != nil {
		return nil
	}
	defer db.Close()

	q := `SELECT context FROM users WHERE id = ?;`
	row, err := db.Query(q, id)
	if err != nil {
		return nil
	}

	defer row.Close()

	for row.Next() {
		var context string
		err = row.Scan(&context)
		if err != nil {

			return nil
		}

		if context != "" {
			return utils.ConvertStrToIntSlice(context)
		}

	}

	return nil
}

func (d *DB) SetContext(id int, context []int) error {
	db, err := sql.Open("sqlite3", d.path+d.name)
	if err != nil {
		return err
	}
	defer db.Close()

	ctx := utils.ConvertIntSliceToString(context)
	q := `UPDATE users SET context = ? WHERE id = ?;`
	_, err = db.Exec(q, ctx, id)
	if err != nil {
		return err
	}

	return nil
}
