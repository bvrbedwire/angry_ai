package db

import (
	"angry_ai/utils"
	"database/sql"
)

func (d *DB) AddToken(token string) error {
	db, err := sql.Open("sqlite3", d.path+d.name)
	if err != nil {
		return err
	}
	defer db.Close()

	q := `INSERT INTO tokens(token) VALUES(?);`
	_, err = db.Exec(q, token, true)
	if err != nil {
		return err
	}

	return nil
}

func (d *DB) AllTokens() string {
	db, err := sql.Open("sqlite3", d.path+d.name)
	if err != nil {
		return ""
	}
	defer db.Close()

	q := `SELECT token FROM tokens;`
	row, err := db.Query(q)
	if err != nil {
		return ""
	}

	defer row.Close()

	res := "TOKENS:\n"

	for row.Next() {
		var token string
		err = row.Scan(&token)
		if err != nil {
			continue
		}
		res += utils.EditForTg(token) + "\n"
	}
	return res
}

func (d *DB) DisableAllTokens() error {
	db, err := sql.Open("sqlite3", d.path+d.name)
	if err != nil {
		return err
	}
	defer db.Close()

	q := `DELETE FROM tokens;`
	_, err = db.Exec(q)
	if err != nil {
		return err
	}

	return nil
}

func (d *DB) DeleteToken(token string) error {
	db, err := sql.Open("sqlite3", d.path+d.name)
	if err != nil {
		return err
	}
	defer db.Close()

	q := `DELETE FROM tokens WHERE token = ?;`
	_, err = db.Exec(q, token)
	if err != nil {
		return err
	}

	return nil
}

func (d *DB) IsAvailableToken(token string) bool {
	db, err := sql.Open("sqlite3", d.path+d.name)
	if err != nil {
		return false
	}
	defer db.Close()

	q := `SELECT token FROM tokens WHERE token = ?;`
	row, err := db.Query(q, token)
	if err != nil {
		return false
	}

	defer row.Close()

	for row.Next() {
		var t string
		err = row.Scan(&t)
		if err != nil {
			return false
		}

		if t != "" {
			return true
		}
	}
	return false
}
