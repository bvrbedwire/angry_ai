package db

import (
	"angry_ai/utils"
	"database/sql"
	"strconv"
)

func (d *DB) AddUser(id int, username string) error {
	db, err := sql.Open("sqlite3", d.path+d.name)
	if err != nil {
		return err
	}
	defer db.Close()

	q := `INSERT INTO users(id, username, is_context, model, req_count, is_enabled) VALUES(?, ?, ?, ?, ?, ?);`
	_, err = db.Exec(q, id, username, false, d.models[0], 0, true)
	if err != nil {
		return err
	}

	return nil
}

// TODO: Make to CSV
func (d *DB) AllUsers() string {
	db, err := sql.Open("sqlite3", d.path+d.name)
	if err != nil {
		return ""
	}
	defer db.Close()

	q := `SELECT id, username, is_enabled, req_count FROM users;`
	row, err := db.Query(q)
	if err != nil {
		return ""
	}

	defer row.Close()

	var res string

	for row.Next() {
		var uid int
		var username string
		var is_enabled bool
		var req_count int
		err = row.Scan(&uid, &username, &is_enabled, &req_count)
		if err != nil {
			continue
		}
		res += strconv.Itoa(uid) + ": " + utils.EditForTg(username) + " " + strconv.FormatBool(is_enabled) + " " + strconv.Itoa(req_count) + "\n"
	}
	return res
}

func (d *DB) DisableUser(id int) error {
	db, err := sql.Open("sqlite3", d.path+d.name)
	if err != nil {
		return err
	}
	defer db.Close()

	q := `UPDATE users SET is_enabled = 0 WHERE id = ?;`
	_, err = db.Exec(q, id)
	if err != nil {
		return err
	}

	return nil
}

func (d *DB) EnableUser(id int) error {
	db, err := sql.Open("sqlite3", d.path+d.name)
	if err != nil {
		return err
	}
	defer db.Close()

	q := `UPDATE users SET is_enabled = 1 WHERE id = ?;`
	_, err = db.Exec(q, id)
	if err != nil {
		return err
	}

	return nil
}

func (d *DB) IsActiveUser(id int) bool {
	db, err := sql.Open("sqlite3", d.path+d.name)
	if err != nil {
		return false
	}
	defer db.Close()

	q := `SELECT is_enabled FROM users WHERE id = ?;`
	row, err := db.Query(q, id)
	if err != nil {
		return false
	}

	defer row.Close()

	for row.Next() {
		var is_enabled bool
		err = row.Scan(&is_enabled)
		if err != nil {
			return false
		}

		if is_enabled {
			return true
		}

	}

	return false
}

// TODO: create the function
func (d *DB) IsUserExists(id int) bool {
	return false
}

func (d *DB) UserInfo(id int) string {
	db, err := sql.Open("sqlite3", d.path+d.name)
	if err != nil {
		return ""
	}
	defer db.Close()

	q := `SELECT username, model, req_count FROM users WHERE id = ?;`
	row, err := db.Query(q, id)
	if err != nil {
		return ""
	}

	defer row.Close()

	var username, model string
	var req_count int
	for row.Next() {
		err = row.Scan(&username, &model, &req_count)
		if err != nil {
			return ""
		}
	}
	return "INFO:\n" + "id: " + strconv.Itoa(id) + "\n" + "username: " + utils.EditForTg(username) + "\nmodel: " + model + "\n" + "requests count: " + strconv.Itoa(req_count)
}

func (d *DB) Model(id int) string {
	db, err := sql.Open("sqlite3", d.path+d.name)
	if err != nil {
		return ""
	}
	defer db.Close()

	q := `SELECT model FROM users WHERE id = ?;`
	row, err := db.Query(q, id)
	if err != nil {
		return d.models[0]
	}

	defer row.Close()

	for row.Next() {
		var model string
		err = row.Scan(&model)
		if err != nil {
			return d.models[0]
		}

		if model != "" {
			return model
		}

	}

	return d.models[0]
}

func (d *DB) ChangeModel(id int, model string) error {
	db, err := sql.Open("sqlite3", d.path+d.name)
	if err != nil {
		return err
	}
	defer db.Close()

	q := `UPDATE users SET model = ? WHERE id = ?;`
	_, err = db.Exec(q, model, id)
	if err != nil {
		return err
	}

	d.ClearContext(id)

	return nil
}

func (d *DB) PlusRequest(id int) error {
	db, err := sql.Open("sqlite3", d.path+d.name)
	if err != nil {
		return err
	}
	defer db.Close()

	q := `UPDATE users SET req_count = req_count + 1 WHERE id = ?;`
	_, err = db.Exec(q, id)
	if err != nil {
		return err
	}
	return nil
}
