package db

import (
	"database/sql"

	_ "github.com/mattn/go-sqlite3"
)

type DB struct {
	name   string
	path   string
	models []string
}

func New(name, path string, m []string) DB {
	return DB{
		name:   name,
		path:   path,
		models: m,
	}
}

func (d *DB) CreateTables() error {
	db, err := sql.Open("sqlite3", d.path+d.name)
	if err != nil {
		return err
	}
	defer db.Close()

	q := `
    CREATE TABLE IF NOT EXISTS users (
        id INTEGER NOT NULL PRIMARY KEY,
		username TEXT,
        context TEXT,
		is_context BOOL NOT NULL,
		model TEXT NOT NULL,
		req_count INTEGER NOT NULL,
		is_enabled BOOL NOT NULL
    );
    `
	_, err = db.Exec(q)
	if err != nil {
		return err
	}

	q = `
    CREATE TABLE IF NOT EXISTS tokens (
        token TEXT NOT NULL UNIQUE
    );
    `
	_, err = db.Exec(q)
	if err != nil {
		return err
	}

	q = `
    CREATE TABLE IF NOT EXISTS settings (
		param TEXT NOT NULL UNIQUE,
        is_enabled BOOL NOT NULL
    );
    `
	_, err = db.Exec(q)
	if err != nil {
		return err
	}

	return nil
}

func (d *DB) AddAdmin(id int, username string) error {
	db, err := sql.Open("sqlite3", d.path+d.name)
	if err != nil {
		return err
	}
	defer db.Close()

	q := `INSERT INTO users(id, username, is_context, model, req_count, is_enabled) VALUES(?, ?, ?, ?, ?, ?);`
	_, err = db.Exec(q, id, "admin", false, d.models[0], 0, true)
	if err != nil {
		return err
	}

	return nil
}

func (d *DB) AddSettingsParams() error {
	db, err := sql.Open("sqlite3", d.path+d.name)
	if err != nil {
		return err
	}
	defer db.Close()

	q := `INSERT INTO settings(param, is_enabled) VALUES(?, ?);`
	_, err = db.Exec(q, "for_users", true)
	if err != nil {
		return err
	}

	return nil
}

func (d *DB) DisableForUsers() error {
	db, err := sql.Open("sqlite3", d.path+d.name)
	if err != nil {
		return err
	}
	defer db.Close()

	q := `UPDATE settings SET is_enabled = 0 WHERE param = ?;`
	_, err = db.Exec(q, "for_users")
	if err != nil {
		return err
	}

	return nil
}

func (d *DB) EnableForUsers() error {
	db, err := sql.Open("sqlite3", d.path+d.name)
	if err != nil {
		return err
	}
	defer db.Close()

	q := `UPDATE settings SET is_enabled = 1 WHERE param = ?;`
	_, err = db.Exec(q, "for_users")
	if err != nil {
		return err
	}

	return nil
}

func (d *DB) IsEnableForUsers() bool {
	db, err := sql.Open("sqlite3", d.path+d.name)
	if err != nil {
		return false
	}
	defer db.Close()

	q := `SELECT is_enabled FROM settings WHERE param = ?;`
	row, err := db.Query(q, "for_users")
	if err != nil {
		return false
	}

	defer row.Close()

	for row.Next() {
		var is_enabled bool
		err = row.Scan(&is_enabled)
		if err != nil {
			return false
		}

		if is_enabled {
			return true
		}

	}

	return false
}

func (d *DB) IsContext(id int) bool {
	db, err := sql.Open("sqlite3", d.path+d.name)
	if err != nil {
		return false
	}
	defer db.Close()

	q := `SELECT is_context FROM users WHERE id = ?;`
	row, err := db.Query(q, id)
	if err != nil {
		return false
	}

	defer row.Close()

	for row.Next() {
		var is_context bool
		err = row.Scan(&is_context)
		if err != nil {
			return false
		}

		if is_context {
			return true
		}

	}
	return false
}
